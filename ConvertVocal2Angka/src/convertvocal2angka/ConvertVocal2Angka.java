/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertvocal2angka;

import java.util.Scanner;

public class ConvertVocal2Angka {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String identitas = "Dinar Fitri Vania Muti / X RPL 6 / 20";
        tampilJudul(identitas);
        String kalimat = tampilInput();
        String convert = vocal2Angka(kalimat);
        tampilPerKata(kalimat, convert);
        tampilHasil(convert);
    }
    
    public static void tampilJudul(String identitas)
    {
        
        System.out.println("Identitas : " + identitas);
        
        System.out.println("\nConvert kalimat alay angka (Vokal ke angka)");
        
    }
    
    public static String tampilInput(){
        Scanner masuk = new Scanner(System.in);
        
        System.out.print("Masukkan kalimat : ");
        String kalimat = masuk.nextLine();
        
        System.out.println("Kalimat asli : " + kalimat);
        
        return kalimat;
        
    }
    
    private static String vocal2Angka(String kalimat)
    {
        char[][]arConvert = {{'a','4'},{'i','1'},{'u','2'},{'e','3'},{'o','0'}};
        kalimat = kalimat.toLowerCase();
        for(int i=0;i<arConvert.length;i++)
            kalimat = kalimat.replace(arConvert[i][0], arConvert[i][1]);
        
        return kalimat;
    }
    
    private static void tampilPerKata(String kalimat, String convert){
        String[] arrKal = kalimat.split(" ");
        String[] arrCon = convert.split(" ");
        
        for(int i=0;i<arrKal.length;i++)
            
            System.out.println(arrKal[i]+"=>"+arrCon[i]);
    }
    
    private static void tampilHasil(String convert){
        
        System.out.print("Kalimat Alay Angka : " + convert);
    }
}
